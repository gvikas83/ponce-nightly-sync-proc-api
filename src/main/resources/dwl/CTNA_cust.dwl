%dw 2.0
//output application/csv quoteValues=true,escape='"'
output application/csv quoteValues=true,escape='"'
import * from dwl::common
---
payload map(value) ->
{
	FinServ__SourceSystemId__c: value."Involved Party Id",
	Address_Verify_Status__c: scrubBlanks(value."Address Verify Status"),
	Onboard_Advisor_Verification_Date__c: toDate1(value."Date Verification"),
	Onboard_Advisor_ID_Verification_Status__c: scrubBlanks(value."Idv Status"),
	Onboard_Advisor_Verification_Status__c: scrubBlanks(value."Verification Status"),
	Onboard_Advisor_Score__c: scrubBlanks(value.Score),
}
