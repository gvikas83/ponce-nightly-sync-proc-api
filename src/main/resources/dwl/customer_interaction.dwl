%dw 2.0
output application/json
import * from dwl::common
---
payload filter ($."Communication Pref 1" != ' ') map(index, value) ->
{
	Customer: value."Customer Key - CIF Key",
	Priority: "1",
	Email_Phone_Type: value."Communication Pref 1 Type",
	Value: value."Communication Pref 1",
	RecordTypeId: RecordType_CustInteration(value."Communication Pref 1 Type"),
	Source_System_Id: value."Customer Key - CIF Key" ++ "-" ++ 
						value."Communication Pref 1 Type" ++ "-" ++
						"1"
}