%dw 2.0
//output application/csv quoteValues=true,escape='"'
output application/json
import * from dwl::common
---
{
	"Financial_Account__r.Account_Number_from_Fiserv__c": (payload."Account Number" default "") ++ "-" ++ payload."Appl Id",
	Account_Type__c: payload."Appl Id",
	Address_Type__c: payload."Title Number",
	Source_System_ID__c: (((payload."Account Number" default "") ++ "-" ++ 
		payload."Appl Id") ++ "-" ++ 
		payload."Title Number") ++ "-" ++ 
		payload."Label Type Desc",
	Sequence_Number__c: payload."Sequence Nbr",
	Address_Line_1__c: payload."Address Line 1",
	Address_Line_2__c: payload."Address Line 2",
	City__c: payload."Address City",
	State__c: payload."Address State",
	PostalCode__c: (payload."Zip Code 5" default "") ++ "-" ++ payload."Zip Code 4",
	Seasonal_Start_Date__c: toDate1(payload."Date Seasonal Start"),
	Seasonal_End_Date__c: toDate1(payload."Date Seasonal End"),
	Label_Type__c: payload."Label Type",
	Label_Type_Desc__c: payload."Label Type Desc"
}