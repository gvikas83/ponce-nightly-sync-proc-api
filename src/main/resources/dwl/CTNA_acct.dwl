%dw 2.0
//output application/csv quoteValues=true,escape='"'
output application/csv quoteValues=true,escape='"'
import * from dwl::common
---
payload map(value) ->
{
	FinServ__LastUpdated__c: toDate1(value."Date Updated Last"),
	FinServ__OpenDate__c: toDate1(value."Date Open"),
	FinServ__PaperlessDelivery__c: if ( value."Statement Option" == 'Paper' ) false else "",
	Account_Number_from_Fiserv__c: (value."Account Number" default "") ++ "-" ++ value."Appl Id",
	New_Account_Status__c: scrubBlanks(value."Account Status"),
	Source_of_Initial_Funding__c: scrubBlanks(value."Source Of Funds")
}
