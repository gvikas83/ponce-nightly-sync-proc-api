%dw 2.0
//output application/csv quoteValues=true,escape='"'
output application/json
fun convertRole(str1, str2) = if ( str1 == "Tax Reporting Owner" ) "Primary Owner"
					  else if ( (str1 == "Additional Owner") and (
					  	(str2 == 'Joint Or') or 
					  	(str2 == 'Co-Borrower'))) "Joint Owner"
					  else str1
import * from dwl::common
---
//payload  map (value) ->
{
	RecordTypeId: vars.RecordTypeId,
	FinServ__Role__c: scrubBlanks(convertRole(payload."Rel Type Desc", payload."Relationship Desc")),
	"FinServ__FinancialAccount__r.Account_Number_from_Fiserv__c": (payload."Account Number" default "") ++ "-" ++
						(payload."Account Service Type" default ""),
	"FinServ__RelatedAccount__r.FinServ__SourceSystemId__c": payload."Customer Key - CIF Key",
	External_ID__c: if ( ['Primary Owner', 'Joint Owner'] contains convertRole(payload."Rel Type Desc", payload."Relationship Desc") ) ((payload."Customer Key - CIF Key" default "") ++ "-" ++
					(payload."Account Number" default "") ++ "-" ++
					(payload."Account Service Type" default "") ++ "-" ++
					(convertRole(payload."Rel Type Desc", payload."Relationship Desc")))
					else ((payload."Customer Key - CIF Key" default "") ++ "-" ++
						(payload."Account Number" default "") ++ "-" ++
						(payload."Account Service Type" default "") ++ "-" ++
						(payload."Rel Type Desc" default "") ++ "-" ++
						(payload."Relationship Desc" default "")),
	Account_Status_Code__c: payload."Account Status Code",
	Account_Service_Type__c: scrubBlanks(payload."Account Service Type"),
	External_Relationship_Type__c: scrubBlanks(payload."External Relationship Type"),
	Frb_Code__c: scrubBlanks(payload."Frb Code"),
	Last_Updated__c: toDate1(payload."Date Updated Last"),
	Relationship_Desc__c: scrubBlanks(payload."Relationship Desc"),
	VRU_Access__c: if ( ['1', '2', '5', '7', '9'] contains payload."Internet Access" ) true else false,
	Internet_Access__c: scrubBlanks(payload."Internet Access"),
	Retail_Online_Full__c: if ( ['1', '3'] contains payload."Internet Access" ) true else false,
	Retail_Online_Credit__c: if ( ['1', '3', '4', '5'] contains payload."Internet Access" ) true else false,
	Retail_Online_Debit__c: if ( ['1', '3', '6', '7'] contains payload."Internet Access" ) true else false,
	Retail_Online_Inquiry__c: if ( ['1', '3', '4', '5', '6', '7', '8', '9'] contains payload."Internet Access" ) true else false,
}
