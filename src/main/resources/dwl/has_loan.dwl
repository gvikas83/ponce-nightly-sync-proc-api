%dw 2.0
//output application/csv quoteValues=true,escape='"'
output application/csv quoteValues=true,escape='"'
---
(payload map (value) ->
{
	FinServ__SourceSystemId__c: value.FinServ__PrimaryOwner__r.FinServ__SourceSystemId__c,
	Has_Loan__c: true
}) distinctBy $.FinServ__SourceSystemId__c