%dw 2.0
//output application/csv quoteValues=true,escape='"'
output application/csv quoteValues=true,escape='"'
import * from dwl::common
---
vars.redFlags filter ($."Record Type Name" == 'Account') map(value) ->
{
	FinServ__Active__c: value.Status match {
		case "Active" -> true
		case "InActive" -> false
		else -> "#N/A"
	},
	FinServ__Message__c: value."Monitored Item Name",
	FinServ__Severity__c: value."Monitored Item Id" match {
		case "1" -> "Warning"
		case "3" -> "Info"
		case "5" -> "Warning"
		case "9" -> "Warning"
		case "10" -> "Info"
		case "11" -> "Info"
		case "18" -> "Warning"
		case "19" -> "Info"
		case "21" -> "Warning"
		case "22" -> "Info"
		case "23" -> "Info"
		case "25" -> "Warning"
		else -> "Info"
	},
//"FinServ__Account__r.FinServ__SourceSystemId__c": value."Involved Party Id",
	"FinServ__FinancialAccount__r.Account_Number_from_Fiserv__c": (value."Account Number" default "") ++ "-" ++
									value."Appl Id"
}
