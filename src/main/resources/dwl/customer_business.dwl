%dw 2.0
//output application/csv quoteValues=true,escape='"'
output json
import * from dw::core::Strings
import * from dwl::common
fun toDate(str) = (str as Date {
	format: "MM/dd/yyyy"
} ++ |00:00:00| ++ "America/Chicago") default "#N/A"
fun toDate1(str) = (str as Date {
	format: "yyyy-MM-dd hh:mm:ss"
} ++ "America/Chicago") default "#N/A"
---
{
	Name: scrubBlanks(capitalize(payload."Customer Full Name")),
	BillingStreet: capitalize((payload."Mailing Address Line 1" default "")) ++ '\n' ++ capitalize(payload."Mailing Address Line 2"),
	BillingCity: scrubBlanks(capitalize(payload."Mailing Address City")),
	BillingStateCode: scrubBlanks(payload."Mailing Address State"),
	BillingPostalCode: scrubBlanks((payload."Mailing Zip" default "") ++ '-' ++ payload."Mailing Plus 4"),
	BillingCountryCode: if ( ['NL', 'PE', 'NS', 'NB', 'QC', 'ON', 'MB', 'SK', 'AB', 'BC', 'YT', 'NT', 'NU'] contains payload."Mailing Address State" ) 'CA' else "US",
	Phone: scrubBlanks(payload."Business Phone"),
	Sic: scrubBlanks(payload."SIC Code"),
	"Current_Branch__r.Branch_Code__c": payload.Branch,
	FinServ__CreditScore__c: scrubBlanks(payload."Credit Score"),
	Date_Service_First__c: toDate(payload."Date Service First"),
	Phone_Data_QA__c: sizeOf(trim(payload."Business Phone")) default 0,
	//Email_Data_QA__c: sizeOf(trim(payload."Customer Email Address")) default 0,
	Secondary_ID_Issuing_State_Province__c: scrubBlanks(payload."State Id Issued Sec"),
	FinServ__SourceSystemId__c: payload."Customer Key - CIF Key",
	Tax_ID_Number__c: scrubBlanks(payload."SS or TIN Number"),
	FinServ__RelationshipStartDate__c: toDate(payload."Date of First Contact"),
	Keyword__c: scrubBlanks(payload.Passkey),
	Preferred_Phone_Number__c: scrubBlanks(payload."Preferred Phone Number"),
	Permission_to_Share_Marketing_Info__c: toBool(payload."Permission to Share Marketing Info"),
	CDD_Cert_Form__c: toBool1(payload."CDD Cert-Form"),
	Classify_Legal_Entity__c: scrubBlanks(payload."Legal Entity"),
	Primary_ID_Issuing_State_Province__c: scrubBlanks(payload."State Id Issued"),
	ID_Number_primary__c: scrubBlanks(payload."Id Number"),
	Secondary_ID_Type__c: scrubBlanks(payload."Id Type Code Sec"),
	Email_Notification__c: scrubBlanks(payload."Email Notification"),
	Business_Date__c: toDate(payload."Business Date"),
	Business_Owner__c: scrubBlanks(payload."Business Owner"),
	Tax_ID_Code__c: if ( payload."Tax ID Code" == '0' ) "None"
								else if ( ["SSN", '5'] contains  payload."Tax ID Code" ) "SSN"
								else if ( payload."Tax ID Code" == "EIN" ) "EIN"
								else if ( ['3', '6'] contains payload."Tax ID Code" ) "Other"
								else if ( ['4', '7'] contains payload."Tax ID Code" ) "Foreign"
								else "#N/A",
	Tax_Identification_Verified__c: if ( ['0', 'SSN', 'EIN', '3', '4'] contains payload."Tax ID Code" ) "True"
								else if ( ['5', '6', '7'] contains  payload."Tax ID Code" ) "False"
								else "#N/A",
	Date_RIB_Accessed__c: toDate1(payload."Date Rib Access"),
	RDC_Ind__c: toBool1(payload."RDC Ind"),
	ACH_Manager__c: toBool1(payload."ACH Manager"),
	ID_Number_secondary__c: scrubBlanks(payload."Id Number Sec"),
	Primary_ID_Type__c: scrubBlanks(payload."Id Type Code"),
	Primary_ID_Expiration_Date__c: toDate1(payload."Date Expiration"),
	Secondary_ID_Expiration_Date__c: toDate1(payload."Date Expiration Sec"),
	Primary_ID_Issued_Date__c: toDate1(payload."Date Id Issued"),
	Secondary_ID_Issued_Date__c: toDate1(payload."Date Id Issued Sec"),
	Primary_ID_Verified_Date__c: toDate1(payload."Date Id Verified"),
	Secondary_ID_Verified_Date__c: toDate1(payload."Date Id Verified Sec"),
	Primary_ID_Issuing_Country__c: scrubBlanks(payload."Country Id Issued"),
	Secondary_ID_Issuing_Country__c: scrubBlanks(payload."Cnty Id Issued Sec"),
	Communication_Preferences__c: scrubBlanks(payload."Text Marketing Opt Out"),
	Last_Updated__c: toDate1(payload."Date Updated Last"),
	SSN_Tax_ID_Last_Four_Digits__c: payload."SS or TIN Number"[-4 to -1] default "xxxx",
	Internet_Bank_Business_Ind__c: toBool1(payload."Internet Bank Business Ind"),
	Date_BIB_Effective__c: toDate1(payload."Date Bib Effective"),
	Internet_Bank_Retail_Ind__c: toBool1(payload."Internet Bank Retail Ind"),
	Date_RIB_Effective__c: toDate1(payload."Date Rib Effective"),
	Date_Bill_Pay_Effective__c: toDate1(payload."Date Bp Effective"),
	RecordTypeId: vars.RecordTypeIdOrg,
	Status_Code__c: if ( payload."Status Code" == 'A' ) 'A'
					else if ( payload."Status Code" == 'I' ) 'I'
					else "#N/A",
	Bill_Pay_Ind__c: toBool1(payload."Bill Pay Ind"),
	Mobiliti_User_Ind__c: toBool(payload."Mobiliti User Ind"),
	Referral_Customer_Code__c: scrubBlanks(payload."Referral Code"),
	Date_First_Mobile_Capture__c: toDate1(payload."Date First Mobile Capture"),
	Occupation_Nature_of_Business__c: scrubBlanks(payload."Occupation Code"),
}
