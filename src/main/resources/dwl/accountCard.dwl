%dw 2.0
output application/java
---
(((payload map(value) -> 
{
	"Card Id": value."Card Id",
	"Account Number": value."Account Number",
	"Appl Id": value."Appl Id"
}) groupBy ($."Card Id" ++ $."Account Number" ++ $."Appl Id"))  mapObject (value) ->
{
	key: (value)[-1]
}).*key