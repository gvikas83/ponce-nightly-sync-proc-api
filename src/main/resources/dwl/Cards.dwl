%dw 2.0
//output application/csv quoteValues=true,escape='"'
output application/csv quoteValues=true,escape='"'
import * from dwl::common
---
payload map(value) ->
{
	Name: value."Card Id",
	FinServ__SourceSystemId__c: value."Card Id" ++ "-" ++ value."Involved Party Id" ++ "-" ++ value."Account Number" ++ "-" ++ value."Appl Id",
	Card_Type__c: scrubBlanks(value."Card Type Code"),
	Card_Status__c: scrubBlanks(value."Card Status Code"),
	Last_Updated__c: toDate2(value."Date Updated Last"),
	Date_Issued__c: toDate2(value."Date Issued"),
	Date_Closed__c: toDate2(value."Date Closed"),
	Date_Used_Last__c: toDate2(value."Date Used Last"),
	"FinServ__FinancialAccount__r.Account_Number_from_Fiserv__c": (value."Account Number" default "") ++ "-" ++ value."Appl Id",
	"FinServ__AccountHolder__r.FinServ__SourceSystemId__c": value."Involved Party Id"
}
