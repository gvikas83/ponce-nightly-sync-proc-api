%dw 2.0
//output application/csv quoteValues=true,escape='"'
output application/csv quoteValues=true,escape='"'
import RecordType_CustInteration from dwl::common
import trim from dw::core::Strings
---
((vars.customer_data filter ($."Communication Pref 6" != ' ') map(value) ->
{
	Customer: value."Customer Key - CIF Key",
	Priority: "6",
	Email_Phone_Type: value."Communication Pref 6 Type",
	Value: value."Communication Pref 6",	
	RecordTypeId: RecordType_CustInteration(value."Communication Pref 6 Type" default "", 
												vars.RecordType_CI_Email,
												vars.RecordType_CI_Phone,
												vars.RecordType_CI_Needs_Fixing),
	Source_System_Id: value."Customer Key - CIF Key" default "" ++ "-" ++ 
						trim(value."Communication Pref 6 Type") default "" ++ "-" ++
						"6"
}) filter ($.Value != ' ')) default []