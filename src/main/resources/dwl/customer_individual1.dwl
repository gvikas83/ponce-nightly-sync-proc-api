%dw 2.0
//output application/csv quoteValues=true,escape='"'
output application/csv quoteValues=true,escape='"'
---
//payload map (value) ->
payload filter ($."Consumer or Business" == 'Con') map (value) ->
{
	RecordTypeId: vars.RecordTypeIdInd,
	"Primary_Account__r.Account_Number_from_Fiserv__c": value."Primary Account"
}
