%dw 2.0
//output application/csv quoteValues=true,escape='"'
output application/csv quoteValues=true,escape='"'
---
(payload map (value) ->
{
	Has_Checking__c: true,
	FinServ__SourceSystemId__c: value.FinServ__PrimaryOwner__r.FinServ__SourceSystemId__c
}) distinctBy $.FinServ__SourceSystemId__c