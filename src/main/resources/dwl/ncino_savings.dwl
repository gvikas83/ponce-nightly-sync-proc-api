%dw 2.0
//output application/csv quoteValues=true,escape='"'
output application/csv quoteValues=true,escape='"'
import * from dwl::common
---
//payload map (value) ->
payload orderBy $."CIF Key of Account Owner" map (value) ->
{
	RecordTypeId: vars.RecordTypeId,
	LLC_BI__Amount__c: value."Current Balance",
	LLC_BI__Interest_Rate__c: (value."Current Interest Rate" as Number) * 100 default "",
	"LLC_BI__Account__r.FinServ__SourceSystemId__c": value."CIF Key of Account Owner",
	LLC_BI__Maturity_Date__c: toDate(value."Next Maturity Date"),
	LLC_BI__Open_Date__c: toDate(value."Open Date"),
	LLC_BI__Product__c: value."Product Major",
	LLC_BI__Status__c: value.Status,
	Name: (value."Account Number" default "") ++ "-" ++ value."Product Major",
	LLC_BI__lookupKey__c: (value."Account Number" default "") ++ "-" ++ value."Product Major",
	LLC_BI__Funding_Amount__c: value."Opening Balance",
	LLC_BI__CD_Term__c: value.Term,
	LLC_BI__Prevent_Automatic_Renewal__c: if ( value."Auto Renew" == "N" ) true
				  else if ( value."Auto Renew" == "Y" ) false
				  else "",
	"LLC_BI__Originating_Branch__r.LLC_BI__lookupKey__c": value."Opening Branch",
	LLC_BI__Term_Type__c: "month(s)",
	LLC_BI__E_Disclosures_And_Statement_Consent__c: toBool(value.eStatements),
	LLC_BI__Electronic_Notice_Delivery_Consent__c: toBool(value.eNotices),
	Consumer_or_Business_Account__c: value."Consumer or Business Account",
}
