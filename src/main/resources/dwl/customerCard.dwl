%dw 2.0
output application/java
---
(((payload map(value) -> 
{
	"Card Id": value."Card Id",
	"Involved Party Id": value."Involved Party Id"
}) groupBy ($."Card Id" ++ $."Involved Party Id"))  mapObject (value) ->
{
	key: (value)[-1]
}).*key