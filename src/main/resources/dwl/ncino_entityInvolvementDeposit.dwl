%dw 2.0
//output application/csv quoteValues=true,escape='"'
output application/csv quoteValues=true,escape='"'
import * from dwl::common
fun convertRole(str) = if(str == "Tax Reporting Owner") "Primary Owner"
					  else str
---
//payload map(value) ->
payload map(value) ->

{
	RecordTypeId: vars.RecordTypeId,
	LLC_BI__Relationship_Type__c: convertRole(value."Rel Type Desc"),
	LLC_BI__lookupKey__c: ((value."Customer Key - CIF Key" default "") ++ value."Account Number") ++ "-" ++  value."Account Service Type",
	Relationship_Desc__c: value."Relationship Desc",
	"LLC_BI__Deposit__r.LLC_BI__lookupKey__c": (value."Account Number" default "") ++ "-" ++ value."Account Service Type",
	"LLC_BI__Account__r.FinServ__SourceSystemId__c": value."Customer Key - CIF Key",
}
