%dw 2.0
fun toDate(str) = (str as Date {format: "MM/dd/yyyy"} ++ |00:00:00| ++ "America/Chicago") default ""
fun toBool(str) = if ( str == "Y" ) true
				  else if ( str == "N" ) false
				  else ""
fun toBool1(str) = if ( str == "0" ) false
				  else if ( str == "1" ) true
				  else ""
fun toDate1(str) = (str as Date {
					format: "yyyy-MM-dd hh:mm:ss"
				} ++ "America/Chicago") default "#N/A"
fun toDate2(str) = (str as Date {
					format: "yyyy-MM-dd 00:00:00"
				} ++ "America/Chicago") default "#N/A"
fun scrubBlanks(str) = if (str == "") "#N/A" else str
fun RecordType_CustInteration(typ: String,rt_email: String, rt_phone: String, rt_none: String) =  
    typ match {
	case "Main Phone" -> rt_phone
	case "Home Phone" -> rt_phone
	case "Cell Phone" -> rt_phone
	case "Business Phone" -> rt_phone
	case "Unknown" -> rt_phone
	case "Business Fax" -> rt_phone
	case "Home Fax"  -> rt_phone
	case "Pager"  -> rt_phone
	case "Personal" -> rt_email
	case "Business" -> rt_email
	case "Other" -> rt_email
	else -> rt_none
}

