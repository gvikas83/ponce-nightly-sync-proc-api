%dw 2.0
//output application/csv quoteValues=true,escape='"'
output application/csv quoteValues=true,escape='"'
---
payload filter ($."Consumer or Business" == 'Bus') map (value) ->
{
	RecordTypeId: vars.RecordTypeIdOrg,
	"Primary_Account__r.Account_Number_from_Fiserv__c": value."Primary Account"
}
